mygithub
====

Backup tool for github  
* clone/pull all stared repositories
* clone/pull all own repositories

## Mirror

* https://git.osuv.de/m/mygithub
* https://gitlab.com/markuman/mygithub

# REQUIREMENTS
 python3, GitPython, requests
  

# USAGE

    ./mygithub.py github_user_name destination/folder
    
