#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 09:09:58 2016

@author: markus

# requirements

* python3
* GitPython
"""

import requests, sys, git, os

VERBOSE = True

def cloning(string):
    if VERBOSE:
        print("clone ... " + string)

def pulling(string):
    if VERBOSE:
        print("pull ... " + string)

def verbose(string):
    if VERBOSE:
        print(string)

def clone(origin, destination, path):
    target = "/".join([path, destination])
    if os.path.exists(target):
        pulling(origin + " to " + target)
        git.Repo(target).remotes.origin.pull()
    else:
        cloning(origin + " to " + target)
        git.Repo.clone_from(origin,  target)
    
    
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("usage: ./mygithub.py github_user_name destination/folder")
        sys.exit(1)
    
    user = sys.argv[1]
    path = sys.argv[2]
    if not os.path.exists(path):
        os.makedirs(path)
        
    verbose("Clone repositories from " + user)
    page = 1
    while True:
        response = requests.get("https://api.github.com/users/" + user + "/repos?page=" + str(page) + "&per_page=100")
        repos = response.json()
        if len(repos) > 0:
            for repo in repos:
                clone(repo['clone_url'], repo['full_name'], path)
            page += 1
        else:
            break
    
    
    verbose("Clone starred repositories from " + user)
    page = 1
    while True:
        response = requests.get("https://api.github.com/users/" + user + "/starred?page=" + str(page) + "&per_page=100")
        repos = response.json()
        if len(repos) > 0:
            for repo in repos:
                clone(repo['clone_url'], repo['full_name'], path)
            page += 1
        else:
            break
