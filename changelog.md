-Version 14
* rewrite in python, because this was totally broken after github changes some html stuff

-Verion 13
 * config option `own=1` to enable backup of you own repositories, `0` for disable
 * config option `stars=1` to enable backup of your stared repositories, `0` for disable

- Version 12
 * fix sed for BSD systems
 * add missing default threads value 2

- Version 11
 * **CAUTION: SYNTAX HAS CHANGED!!**
 * fix bug when pulling parallel
 * config ~/.mygithub.cfg added

- Version 10
 * using GNU parallel _(optional)_
  * edit line 26 to control threads
  * default threads are 2 _(if you've got a high bandwidth or running `mygithub` on a 1gibt server, rise it up to >=10)_
  * is you set threads to 0, parallel methode will be disabled

- Version 9
 * fix MAC OS mktemp again
 * fix [SC2089](https://github.com/koalaman/shellcheck/wiki/SC2086)
 * prepare for parallel pull/clone
 * remove version changelog from script itself

- Version 8
 * MAC OS compatible mktemp -dt option

- Version 7
 * clone -recursive by default

- Verion 6
 * improve third option input. it's set to "justme" now.

- Version 5
 * support mirroring only repositories from given username
 * e.g. $ ./mygithub markuman /mnt/GITHUB/ justme
 * it's a weak implementation, because it doesn't matter what kind of string the third option is!

- Version 4
 * remove cat dependency
 * fix bug while extracting own repositories

- Version 3
 * rename cmgs to mygithub
 * clone/pull stars and own github repositories
 * mkdir -p for destination dir

- Version 2
 * implement pull if repository already exist

- Version 1
 * initial release

